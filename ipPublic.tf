resource "azurerm_public_ip" "ArkTerraformPublicIp" {
    name                         = "ArkPublicIP"
    location                     = "eastus"
    resource_group_name          = "${azurerm_resource_group.arktfGroup.name}"
    public_ip_address_allocation = "dynamic"

    tags {
        environment = "Terraform Demo"
    }
}
