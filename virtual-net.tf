resource "azurerm_virtual_network" "ArkTerraformNetwork" {
    name                = "ArkVnet"
    address_space       = ["10.0.0.0/16"]
    location            = "eastus"
    resource_group_name = "${azurerm_resource_group.arktfGroup.name}"

    tags {
        environment = "Ark Terraform VNetDemo"
    }
}
