resource "random_id" "ArkrandomId" {
    keepers = {
        #Generate a new ID only when a new resource group is defined
        resource_group = "${azurerm_resource_group.arktfGroup.name}"
    }

    byte_length = 8
}


resource "azurerm_storage_account" "ArkStorageAccount" {
    name                = "diag${random_id.ArkrandomId.hex}"
    resource_group_name = "${azurerm_resource_group.arktfGroup.name}"
    location            = "eastus"
    account_replication_type = "LRS"
    account_tier = "Standard"

    tags {
        environment = "Terraform Demo"
    }
}
