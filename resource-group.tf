resource "azurerm_resource_group" "arktfGroup" {
    name     = "ArkResourGroup"
    location = "east us"

    tags {
        environment = "Ark Terraform Demo"
    }
}
