resource "azurerm_network_interface" "ArkTerraformNIC" {
    name                = "ArkNIC"
    location            = "eastus"
    resource_group_name = "${azurerm_resource_group.arktfGroup.name}"
    network_security_group_id = "${azurerm_network_security_group.ArkTerraformNsg.id}"

    ip_configuration {
        name                          = "ArkNicConfiguration2"
        subnet_id                     = "${azurerm_subnet.ArkTerraformSubnet2.id}"
        private_ip_address_allocation = "dynamic"
        public_ip_address_id          = "${azurerm_public_ip.ArkTerraformPublicIp.id}"
    }

    tags {
        environment = "Terraform Demo"
    }
}
