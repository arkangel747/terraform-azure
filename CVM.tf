resource "azurerm_virtual_machine" "ArkTerraformVM" {
    name                  = "ArkVM"
    location              = "eastus"
    resource_group_name   = "${azurerm_resource_group.arktfGroup.name}"
    network_interface_ids = ["${azurerm_network_interface.ArkTerraformNIC.id}"]
    vm_size               = "Standard_DS1_v2"

    storage_os_disk {
        name              = "ArkOsDisk"
        caching           = "ReadWrite"
        create_option     = "FromImage"
        managed_disk_type = "Premium_LRS"
    }

    storage_image_reference {
        publisher = "Canonical"
        offer     = "UbuntuServer"
        sku       = "16.04.0-LTS"
        version   = "latest"
    }

    os_profile {
        computer_name  = "Arkvm"
        admin_username = "arkangel"
    }

    os_profile_linux_config {
        disable_password_authentication = true
        ssh_keys {
            path     = "/home/arkangel/.ssh/authorized_keys"
            key_data = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC9XyWS5iVHskK0Ws22D1bUYj8iRk2TcFcmttQCpm3dHBavrIi9Inbm7NCFuDd93Aj/zxBRHJcz08pocoXDqcE8HSf7+prk4qAwABjQIW0RhhW2b8fCx0lyea12zEqTz8EWyGQ061wefVsa1IGxgGroPvNvj5SRNH9tr/ndcdDQXAbyd9quU1aO5qX5pBdqNnKF58VGX8zQr3+4Y8+zKBEAHBUmny9zAYxZ+d2/tTNEs5Y/edKbOTpuhv6982URYXOXnsreFgZzPjU064bLjfXaPDgRdPcTwGtQahbUxCBmujn0FhPAEmJ39Ty2ehDn2gniEpYwBEqLHPwL2VSqlThj root@Pana"
        }
    }

    #boot_diagnostics {
    #    enabled     = "true"
    #    storage_uri = "${azurerm_storage_account.mystorageaccount.primary_blob_endpoint}"
    #}

    tags {
        environment = "Terraform Demo"
    }
}
