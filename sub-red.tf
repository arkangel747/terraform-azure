resource "azurerm_subnet" "ArkTerraformSubnet2" {
    name                 = "ArkSubnet2"
    resource_group_name  = "${azurerm_resource_group.arktfGroup.name}"
    virtual_network_name = "${azurerm_virtual_network.ArkTerraformNetwork.name}"
    address_prefix       = "10.0.2.0/24"
}

resource "azurerm_subnet" "ArkTerraformSubnet3" {
    name                 = "ArkSubnet3"
    resource_group_name  = "${azurerm_resource_group.arktfGroup.name}"
    virtual_network_name = "${azurerm_virtual_network.ArkTerraformNetwork.name}"
    address_prefix       = "10.0.3.0/24"
}

resource "azurerm_subnet" "ArkTerraformSubnet4" {
    name                 = "ArkSubnet4"
    resource_group_name  = "${azurerm_resource_group.arktfGroup.name}"
    virtual_network_name = "${azurerm_virtual_network.ArkTerraformNetwork.name}"
    address_prefix       = "10.0.4.0/24"
}
